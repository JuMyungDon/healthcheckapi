package com.md.healthcheckapi.controller;

import com.md.healthcheckapi.model.MachineRequest;
import com.md.healthcheckapi.model.MachineStaticsResponse;
import com.md.healthcheckapi.service.MachineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {
    private final MachineService machineService;

    @PostMapping("/new")
    public String setMachine(@RequestBody MachineRequest request) {
        machineService.setMachine(request);

        return "ok";
    }

    @GetMapping("/statics")
    public MachineStaticsResponse getStatics() {
        return machineService.getStatics();
    }
}
