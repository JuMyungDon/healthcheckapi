package com.md.healthcheckapi.controller;

import com.md.healthcheckapi.model.*;
import com.md.healthcheckapi.service.HealthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping("/people")
    public String setHealth(@RequestBody HealthRequest request) {
        healthService.setHealth(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<HealthItem> getHealths(){
        return healthService.getHealths();
    }

    @GetMapping("/detail/{id}")
    public HealthResponse getHealth(@PathVariable long id){
        return healthService.getHealth(id);
    }

    @PutMapping("/status/{id}")
    public String putHealthStatus(@PathVariable Long id,@RequestBody HealthStatusChangeRequest request) {
        healthService.putHealthStatus(id, request);

        return "ok";
    }

    @PutMapping("/base-info/{id}")
    public String putHealthBaseInfo(@PathVariable Long id, @RequestBody HealthBaseInfoChangeRequest request) {
        healthService.putBaseInfo(id, request);

        return "ok";
    }
}
