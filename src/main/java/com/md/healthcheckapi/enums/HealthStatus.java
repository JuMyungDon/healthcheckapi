package com.md.healthcheckapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HealthStatus {
    FEEL_GOOD("좋음", false),
    NORMAL("보통", false),
    SICK("아픔", true),
    BE_DEAD("죽음의 문턱", true);

    private final String name;
    private final Boolean isEarlyLeave;
}
