package com.md.healthcheckapi.model;

import com.md.healthcheckapi.enums.HealthStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthRequest {
    private String name;

    @Enumerated(value = EnumType.STRING)
    private HealthStatus healthStatus;

    private Boolean isChronicDisease;

    private String etcMemo;
}
