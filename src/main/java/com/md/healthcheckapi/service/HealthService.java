package com.md.healthcheckapi.service;

import com.md.healthcheckapi.entity.Health;
import com.md.healthcheckapi.enums.HealthStatus;
import com.md.healthcheckapi.model.*;
import com.md.healthcheckapi.repository.HealthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HealthService {
    private final HealthRepository healthRepository;

    public void setHealth(HealthRequest request) {
        Health addData = new Health();
        addData.setDateCreate(LocalDate.now());
        addData.setName(request.getName());
        addData.setHealthStatus(request.getHealthStatus());
        addData.setIsChronicDisease(request.getIsChronicDisease());
        addData.setEtcMemo(request.getEtcMemo());

        healthRepository.save(addData);
    }

    public List<HealthItem> getHealths() {
        List<Health> originList = healthRepository.findAll();

        List<HealthItem> result = new LinkedList<>();

        for (Health health : originList) { // 향상된 for
            HealthItem addItem = new HealthItem();
            addItem.setId(health.getId());
            addItem.setDateCreate(health.getDateCreate());
            addItem.setName(health.getName());
            addItem.setHealthStatus(health.getHealthStatus().getName());
            addItem.setIsEarlyLeave(health.getHealthStatus().getIsEarlyLeave());

            result.add(addItem);
        }

        return result;
    }

    public HealthResponse getHealth(long id) {
        Health originData = healthRepository.findById(id).orElseThrow();

        HealthResponse response = new HealthResponse();
        response.setId(originData.getId());
        response.setHealthStatusName(originData.getHealthStatus().getName());

        // 3항연산자 => 간략한 if문
        response.setIsEarlyLeaveName(originData.getHealthStatus().getIsEarlyLeave() ? "예" : "아니오");

        response.setDateCreate(originData.getDateCreate());
        response.setName(originData.getName());

        response.setIsChronicDiseaseName(originData.getIsChronicDisease() ? "예" : "아니오");

        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putHealthStatus(Long id, HealthStatusChangeRequest request) {
        Health originData = healthRepository.findById(id).orElseThrow();
        originData.setHealthStatus(request.getHealthStatus());

        healthRepository.save(originData);
    }

    public void putBaseInfo(Long id, HealthBaseInfoChangeRequest request) {
        Health originData = healthRepository.findById(id).orElseThrow();
        originData.setName(request.getName());
        originData.setIsChronicDisease(request.getIsChronicDisease());

        healthRepository.save(originData);
    }
}