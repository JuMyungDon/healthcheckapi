package com.md.healthcheckapi.service;

import com.md.healthcheckapi.entity.Machine;
import com.md.healthcheckapi.model.MachineRequest;
import com.md.healthcheckapi.model.MachineStaticsResponse;
import com.md.healthcheckapi.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public void setMachine(MachineRequest request) {
        Machine addData = new Machine();
        addData.setMachineName(request.getMachineName());
        addData.setMachinePrice(request.getMachinePrice());
        addData.setDateBuy(request.getDateBuy());

        machineRepository.save(addData);
    }

    public MachineStaticsResponse getStatics() {
        MachineStaticsResponse response = new MachineStaticsResponse();

        List<Machine> originList = machineRepository.findAll();

        double totalPrice = 0D;
        double totalPressKg = 0D;

        for (Machine machine : originList) { // 향상된 for
            // java 연산자
            totalPrice += machine.getMachinePrice();
            totalPressKg += machine.getMachineType().getPressKg();
            // totalPrice = totalPrice + machine.getMachinePrice();
        }

        double avgPrice = totalPrice / originList.size();

        response.setTotalPrice(totalPrice);
        response.setAveragePrice(avgPrice);
        response.setTotalPressKg(totalPressKg);

        return response;
    }
}
