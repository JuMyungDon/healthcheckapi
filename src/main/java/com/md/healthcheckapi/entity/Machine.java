package com.md.healthcheckapi.entity;

import com.md.healthcheckapi.enums.MachineType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Machine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private MachineType machineType;

    @Column(nullable = false, length = 30)
    private String machineName;

    @Column(nullable = false)
    private Double machinePrice;

    @Column(nullable = false)
    private LocalDate dateBuy;
}
