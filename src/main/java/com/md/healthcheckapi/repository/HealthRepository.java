package com.md.healthcheckapi.repository;

import com.md.healthcheckapi.entity.Health;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRepository extends JpaRepository<Health, Long> {//창고지기
}
