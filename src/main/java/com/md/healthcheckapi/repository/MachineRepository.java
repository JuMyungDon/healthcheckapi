package com.md.healthcheckapi.repository;

import com.md.healthcheckapi.entity.Machine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MachineRepository extends JpaRepository<Machine, Long> {
}
